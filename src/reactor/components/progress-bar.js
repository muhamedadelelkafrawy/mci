import React from 'react';
import { LinearProgress, ThemeProvider, createMuiTheme } from "@material-ui/core";
import styleSettings from 'reactor/layout/utils/style-settings';

export default function ProgressBar() {
    let primaryColor = styleSettings.get('colors.primary');

    let settings = {
        palette: {
            primary: {}
        }
    };

    if (primaryColor) {
        settings.palette.primary.main = styleSettings.get('colors.primary');
    }

    const theme = createMuiTheme(settings);
    return (
        <ThemeProvider theme={theme}>
            <LinearProgress />
        </ThemeProvider>
    )
}