import { createBrowserHistory } from 'history';
import config from '../config';

const history = createBrowserHistory({
    basename: process.env.NODE_ENV === 'production' ? config.get('basePath', '/') : '/',
});

export default history;