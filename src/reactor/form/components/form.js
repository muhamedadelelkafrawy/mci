import React from 'react';
import '../validation/locales/en';
import '../validation/locales/ar';
import { Arr } from 'reinforcements';
import serialize from 'form-serialize';
import FormContext from '../utils/form-context';
import ReactorComponent from 'reactor/components/reactor.component';

export default class Form extends ReactorComponent {
    inputs = [];
    isValidForm = true;
    formElement = null;
    dirtyInputs = new Arr([]);
    state = {}

    setInput(input) {
        if (input.id && this.inputs.find(inp => inp.id === input.id)) {
            let inputIndex = this.inputs.findIndex(inp => inp.id === input.id);

            this.inputs[inputIndex] = input;

            return;
        }

        if (this.inputs.includes(input)) return;

        this.inputs.push(input);
    }

    /**
     * Submit form
     */
    triggerSubmit(e) {
        e.preventDefault();
        e.stopPropagation();

        this.isValidForm = true; // make sure its is reset

        for (let input of this.inputs) {
            input.validate(e);

            if (input.hasError) {
                this.isValidForm = false;
            }
        }
        
        // check if the form is valid
        // if not, then do not submit
        if (this.isValidForm === false) return;

        if (this.props.onSubmit) {
            this.isSubmitting = true;
            // this.setState({
            //     isSubmitting: true,
            // });
            this.props.onSubmit(e, this);
        }
    }

    /**
     * Convert form to query string
     * 
     * @return {string} 
     */
    toQueryString() {
        return serialize(this.formElement);
    }

    /**
     * Set form submission state
     * 
     * @param  boolean submitting
     */
    submitting(submitting) {
        this.isSubmitting = submitting;
        this.set('submitting', submitting);
    }

    /**
     * Convert current form into plain object
     * 
     * @returns {object} 
     */
    toObject() {
        return serialize(this.formElement, true);
    }

    cleanInput(input) {
        this.dirtyInputs.remove(input);

        setTimeout(() => {
            this.isValidForm = this.dirtyInputs.isEmpty();
            this.forceUpdate();
        }, 0);
    }

    dirtyInput(input) {
        this.dirtyInputs.pushOnce(input);
        
        this.isValidForm = false;

        setTimeout(() => {
            this.forceUpdate();
        }, 0);
    }

    /**
     * Trigger form submission programmatically
     * 
     * @returns {void} 
     */
    submit() {
        this.formElement.requestSubmit();
    }

    /**
     * {@inheritdoc}
     */
    render() {
        // noValidate disables the browser default validation
        return (
            <FormContext.Provider value={{ form: this }}>
                <form ref={form => this.formElement = form} noValidate={this.props.noValidate} onSubmit={this.triggerSubmit.bind(this)}>
                    {this.children()}
                </form>
            </FormContext.Provider>
        );
    }
}

Form.defaultProps = {
    noValidate: true,
};