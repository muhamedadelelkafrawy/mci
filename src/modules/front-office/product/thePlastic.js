import React from 'react';
import Plastic from '../../../assets/images/plastic.jpg'
import Coming from '../../../assets/images/com-soon.png'
// import QuoteButton from './quoteButton';
import { Button, Checkbox, FormControlLabel, styled } from '@material-ui/core';
import cls from 'clsx';

export default function ThePlastic() {
    const [checked, setChecked] = React.useState(false);
    const [types, setTypes] = React.useState('');

    const isSelectedType = theType => cls({
        'normalSelection': true,
        activeSelection: types === theType
    })

    const handleChange = (event) => {
        setChecked(event.target.checked);
    };

    const typesList = [
        'Child',
        'PP Caps',
        'PP Plastic Caps',
    ]

    const CustomButton = styled(Button)({
        outline: 'none',
        border: ' none',
    })

    return (
        <>
            <div className="the-main-container">
                <div className="the-type-description">
                    <p>High quality of raw material polypropylene or as your request raw material, suitable with FDA & food base. Outer, according requirements of client with more than 10 colors of logo, suitable with FDA & food base.</p>
                </div>
                <div className='quote-content'>
                    <div className='quote-content-left-part'>
                        <img src={Plastic} className='quote-image' alt='quote' />
                    </div>
                    <div className='quote-content-right-part1'>
                        <div className='coming-soon-plastic-div'>
                            <img src={Coming} className='coming-quote' alt='quote' />
                        </div>
                        <div className='big-right-part'>
                            <div className='standard'>
                                <div className='standard-header'>
                                    <h1>Types</h1>
                                </div>
                                <div className='standard-body'>
                                    <div className='the-comparison'>
                                        <div className='the-comparison-content'>
                                            {typesList.map(type => (
                                                <CustomButton className={isSelectedType(type)} onClick={() => setTypes(type)}>{type} </CustomButton>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='standard'>
                                <div className='standard-header'>
                                    <h1>Logo Availability</h1>
                                </div>
                                <div className='standard-body'>
                                    <div className='the-comparison-content'>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={checked}
                                                    onChange={handleChange}
                                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                                    color='primary'
                                                />
                                            }
                                            label="Print Logo"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <div className='button-container'>
                        <QuoteButton checked={checked} types={types} />
                    </div> */}
                    </div>
                </div>
            </div>
        </>
    )
}