import React from 'react';
import ThePilfer from './thePilfer';
import TheFlip from './theFlip';
import ThePlastic from './thePlastic';
import TheAlumunium from './theAlumunium';
import cls from 'clsx';
import For from 'reactor/For';

const productsList = {
    pilfer: ThePilfer,
    flip: TheFlip,
    plastic: ThePlastic,
    alumunium: TheAlumunium,
}

const typeCardsButtons = [
    {
        text: 'Pilfer Proof Caps',
        value: 'pilfer',
    },
    {
        text: 'Flip-Off Caps',
        value: 'flip',
    },
    {
        text: 'Alumunium Caps',
        value: 'alumunium',
    },
    {
        text: 'Plastic Caps',
        value: 'plastic',
    },
]

export default function TypesCategory(props) {

    const [status, setStatus] = React.useState(props.name || 'pilfer');
    const DifferentProductTypes = status ? productsList[status] : React.Fragment;

    const typeClass = type => cls({
        'type-card': true,
        activeOne: status === type
    })

    return (
        <>
            <div className='types-part'>
                <For array={typeCardsButtons} render={type => (
                    <div  className={typeClass(type.value)}>
                        <h2 onClick={() => setStatus(type.value)} className='type-card-style'>{type.text}</h2>
                    </div>
                )} />
            </div>
            <div className='big-quote'>
                <DifferentProductTypes />
            </div>
        </>
    )
}