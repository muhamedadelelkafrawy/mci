import React from 'react';
import { Button, styled } from '@material-ui/core';
import QuoteContactForm from './quoteContactForm';

const AddButton = styled(Button)({
    cursor: 'pointer',
    backgroundColor: '#F0F0F0',
    borderRadius: '25px',
    border: ' 2px solid #1F4B66',
    color: '#EA8E4A',
    fontSize: '20px'
})
export default function QuoteForm({ color, thickness, height, checked, dia, type, closePopup }) {
    let validType = false;
    switch (type) {
        case 'pilfer':
        case 'flip':
            validType = color && height && dia && thickness;
            break;
        case 'alumunium':
            validType = thickness && height && dia;
            break;
    }
    return (
        <>
            <div className="popup-box1">
                <div className='quote-form-container1'>
                    <div className='quote-form-header1'>
                        {!validType &&
                            <div className='no-items-div'>
                                <h3>No Items are Selected, Please Select Some Products</h3>
                                <AddButton onClick={() => closePopup()} variant="contained" color="primary" className="add-item-btn">
                                    Add Item
                                </AddButton>
                            </div>
                        }
                        {validType &&
                            <div className='data-sent'>
                                {dia && <h2> Diameter: <strong> {dia} mm</strong> </h2>}
                                {height && <h2> Height: <strong>{height} mm</strong></h2>}
                                {thickness &&<h2> Thickness: <strong>{thickness} mm</strong></h2>}
                                {/* <h2> Types: <strong>{types} mm</strong></h2> */}
                                {color &&<h2> Color Code: <strong>{color}</strong></h2>}
                                {checked &&<h2>{checked && <strong>Need A Logo</strong>}</h2>}
                            </div>
                        }

                    </div>
                    <div className='quote-form-contact-info'>
                        <QuoteContactForm hiddenInputs={{ color, thickness, height, checked, dia }} />
                    </div>
                </div>
            </div>
        </>
    )
}