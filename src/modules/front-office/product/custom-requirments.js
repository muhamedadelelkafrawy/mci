import React from 'react';
import { Checkbox, FormControlLabel, InputAdornment, TextField } from '@material-ui/core';
import { SketchPicker } from 'react-color';

// const NextButton = styled(Button)({
//     cursor: 'pointer',
//     backgroundColor: '#F0F0F0',
//     borderRadius: '25px',
//     marginRight: '10px',
//     color: 'black',
//     '&:hover': {
//         background: "#1F4B66",
//         color: 'white'
//     },
// })
export default function CustomRequirementsForm({quotation, updateQuotation, type}) {
    console.log(type);
    const [color, setColor] = React.useState({
        hex: null
    });
    // const [coreDia, setCoreDIa] = React.useState('')
    // const [height, setHeight] = React.useState('');
    // const [thickness, setThickness] = React.useState('');
    const [checked, setChecked] = React.useState(false);

    const handleChange = (event) => {
        setChecked(event.target.checked);
        update('checked', event.target.checked);
    };

    const update = (name, value) => {
        updateQuotation({...quotation, [name]: value});
    }

    return (
        <>
            <div className=''>
                <div className=''>
                    <h3>*Please Fill the bellow fields with your custom requirements</h3>
                    <div className='custom-input-fields'>
                        <TextField
                            required
                            label="Core Diameter"
                            type="number"
                            InputProps={{
                                endAdornment: <InputAdornment position="end">mm</InputAdornment>,
                            }}
                            onChange={e  => update('dia', e.target.value)}
                        />
                        <TextField
                            required
                            label="Height"
                            type="number"
                            InputProps={{
                                endAdornment: <InputAdornment position="end">mm</InputAdornment>,
                            }}
                            onChange={e  => update('height', e.target.value)}
                        />
                        <TextField
                            required
                            label="Thickness"
                            type="number"
                            InputProps={{
                                endAdornment: <InputAdornment position="end">mm</InputAdornment>,
                            }}
                            onChange={e  => update('thickness',e.target.value)}
                        />
                    </div>
                    {type !== 'alumunium' && 
                    <div className="custom-input-fields">
                        <SketchPicker className='colored' color={color} onChange={color => {setColor(color); update('color', color.hex)}} />
                        <div className="print-logo">
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={checked}
                                        onChange={handleChange}
                                        inputProps={{ 'aria-label': 'primary checkbox' }}
                                        color='primary'
                                    />
                                }
                                label="Print Logo"
                            />
                        </div>
                    </div>
                    }
                </div>
            </div>
        </>
    )
}