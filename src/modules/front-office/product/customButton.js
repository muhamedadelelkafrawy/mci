import React from 'react';
import QuoteForm from './quote-form';
import CustomRequirementsForm from './custom-requirments';
import { Button, styled } from '@material-ui/core';
import Modal from 'reactor/components/modal';

const CustomRequirementsButton = styled(Button)({
    cursor: 'pointer',
    backgroundColor: '#EA8E4A',
    borderRadius: '25px',
    marginRight: '10px',
    '&:hover': {
        background: "#e27802",
    },
})
const NextButton = styled(Button)({
    cursor: 'pointer',
    backgroundColor: '#F0F0F0',
    borderRadius: '25px',
    marginRight: '10px',
    color: 'black',
    '&:hover': {
        background: "#1F4B66",
        color: 'white'
    },
})

export default function TheCustomButton({ type }) {
    const customToggleForm = () => {
        openModal(!customIsOpen);
    }
    const [customIsOpen, openModal] = React.useState(false);
    const [viewing, view] = React.useState('quotation');
    const [quotation, setQuotation] = React.useState({
        color: null,
        height: null,
        thickness: null,
        dia: null,
        checked: false,
    })
    const closePopup = () => {
        openModal(false);
        view('quotation');
        setQuotation({
            color: null,
            height: null,
            thickness: null,
            dia: null,
            checked: false,
        })
    };
    return (
        <>
            <div className='quote-btn'>
                <CustomRequirementsButton variant="contained" color="primary" onClick={customToggleForm}>
                    Custom Requirements
                </CustomRequirementsButton>
            </div>

            <Modal
                open={customIsOpen}
                size="md"
                backdrop
                esc
                onClose={closePopup}
            >
                {viewing === 'quotation' &&
                    <>
                        <CustomRequirementsForm type={type} updateQuotation={setQuotation} quotation={quotation} />
                        <div className='next-button'>
                            <NextButton onClick={() => view('contact')} variant="contained" color="primary" >
                                Next
                            </NextButton>
                        </div>
                    </>
                }
                {viewing === 'contact' &&
                    <QuoteForm {...quotation} closePopup={closePopup} type={type} />
                }
            </Modal>
        </>

    )
}

