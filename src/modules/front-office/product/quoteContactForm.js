import React from 'react';
import { Button, TextField, makeStyles } from '@material-ui/core';
import QuoteList from './quoteList';
import For from 'reactor/For';
import Axios from 'axios';
import Checkbox from 'reactor/form/components/checkbox';
import Form from 'reactor/form/components/form';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            backgroundColor: 'white',
        },
    },
}));


export default function QuoteContactForm({ hiddenInputs }) {
    const classes = useStyles();
    const [isSuccess, setSuccess] = React.useState();

    const formSubmit = e => {
        Axios.post('./contactus.php', new FormData(e.target)).then(response => {
            setSuccess(true);
        })
    };

    if (isSuccess) {
        return (
            <h3 className="successMessage">Your Request Is Submitted Successfully </h3>
        )
    }
    return (
        <>
            <Form onSubmit={formSubmit}>
                <div className="quote-contact-form">
                    <h1 className="contact-header-font">CONTACT INFO</h1>
                    <For object={hiddenInputs} render={(key, value) => (
                        <input type="hidden" name={key} value={value} />
                    )} />
                    <div className={classes.root} noValidate autoComplete="off">
                        <TextField required name="name" className='quote-input-field' label="Full Name" variant="outlined" />
                        <TextField required name="email" className='quote-input-field' label="Email Address" variant="outlined" />
                    </div>
                    <div className={classes.root} noValidate autoComplete="off">
                        <TextField required name="phoneNumber" className='quote-input-field' type='number' label="Phone Number" variant="outlined" />
                        <TextField required name="companyName" className='quote-input-field' label="Company Name" variant="outlined" />
                    </div>
                    <div className={classes.root} noValidate autoComplete="off">
                        <TextField name="message" rows={10} multiline={true} className='quote-input-field-long' label="Your Message" variant="outlined" />
                    </div>
                    <Checkbox name="requestSample" label="Request Sample" />
                    <Checkbox name="requestVisit" label="Request Visit" />
                    {/* <QuoteList /> */}
                    <div className={classes.root} noValidate autoComplete="off">
                        <Button type="submit" variant="contained" className='quote-submit-btn'>
                            Request
                    </Button>
                    </div>
                </div>
            </Form>
        </>
    )
}