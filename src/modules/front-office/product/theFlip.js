import React from 'react';
import Flip from '../../../assets/images/FLIP2.jpg'
import QuoteButton from './quoteButton';
import TheCustomButton from './customButton';
import { SketchPicker } from 'react-color';
import { Button, Checkbox, FormControlLabel, styled } from '@material-ui/core';
import cls from 'clsx';

const heights = [
    5,
    6,
    7.3,
    7.4,
    7.5,
    7.6,
    7.7,
    7.8,
    7.9,
    8,
    9,
    9.3,
    9.5,
    9.7,
    9.9,
    10,
    10.5,
    11,
    11.5,
    12,
    12.5,
    13,
    13.5,
    14,
    14.5,
    15,
];

const thicknessList = [
    0.18,
    0.19,
    0.20,
    0.21,
    0.22,
    0.23,
    0.24,
    0.25,
]

const coreDiaList = [
    {
        value: 8,
        minHeight: 5,
        maxHeight: 5,
    },
    {
        value: 13,
        minHeight: 5,
        maxHeight: 6,
    },
    {
        value: 20,
        minHeight: 7.3,
        maxHeight: 8,
    },
    {
        value: 32,
        minHeight: 9,
        maxHeight: 15,
    },
]

const CustomButton = styled(Button)({
    outline: 'none',
    textTransform: 'lowercase',
    border: ' none',
})

export default function TheFlip() {

    const [checked, setChecked] = React.useState(false);
    const handleChange = (event) => {
        setChecked(event.target.checked);
    };
    const [color, setColor] = React.useState({
        hex: null
    });
    const [height, setHeight] = React.useState('');
    const [coreDia, setCoreDIa] = React.useState({
        minHeight: 0,
        maxHeight: 0,
    }); // initial no selection
    const [thickness, setThickness] = React.useState('');

    const isSelectedDia = theDia => cls({
        'normalSelection': true,
        activeSelection: coreDia === theDia
    })

    const isSelectedHeight = theHeight => cls({
        'normalSelection': true,
        activeSelection: height === theHeight
    })

    const isSelectedThickness = theThickness => cls({
        'normalSelection': true,
        activeSelection: thickness === theThickness
    })


    return (
        <>
            <div className="the-main-container">
                <div className="the-type-description">
                    <p>FDA coating inside and outside , gold color or silver , with logo printing or Carved injection with plastic part (as request), Suitable for all types of pharmaceutical vials, and suitable for all sterilization operations by steam, gamma, and autoclave.</p>
                </div>
                <div className='quote-content'>
                    <div className='quote-content-left-part'>
                        <img src={Flip} className='quote-image' alt='quote ' />
                    </div>
                    <div className='quote-content-right-part'>
                        <div className='big-right-part'>
                            <div className='standard'>
                                <div className='standard-header'>
                                    <h1>Dimensions</h1>
                                    <div className='small-types-comparison'>
                                        <p>core dia</p>
                                        <p>H(height)</p>
                                    </div>
                                </div>
                                <div className='standard-body'>
                                    <div className='the-comparison'>
                                        <div className='the-comparison-content'>
                                            {coreDiaList.map(diameter => (
                                                <CustomButton className={isSelectedDia(diameter)} onClick={() => setCoreDIa(diameter)}>{diameter.value} mm</CustomButton>
                                            ))}
                                        </div>
                                        <div className="quote-vl"></div>
                                        <div className="the-comparison-content">
                                            {heights.map(height => (
                                                <CustomButton className={isSelectedHeight(height)} onClick={() => setHeight(height)} type="button" disabled={height < coreDia.minHeight || height > coreDia.maxHeight}>{height} mm</CustomButton>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='standard'>
                                <div className='standard-header'>
                                    <h1>Thickness</h1>
                                </div>
                                <div className='standard-body'>
                                    <div className='the-comparison-content'>
                                        {thicknessList.map(thick => (
                                            <CustomButton className={isSelectedThickness(thick)} onClick={() => setThickness(thick)} type="button">{thick} mm</CustomButton>
                                        ))}
                                    </div>
                                </div>
                            </div>
                            <div className='standard'>
                                <div className='standard-header'>
                                    <h1>Color Palette</h1>
                                </div>
                                <div className='standard-body'>
                                    <div className='the-comparison-content'>
                                        <SketchPicker className='colored' color={color} onChange={updatedColor => setColor(updatedColor)} />
                                    </div>
                                </div>
                            </div>
                            <div className='standard'>
                                <div className='standard-header'>
                                    <h1>Logo Availability</h1>
                                </div>
                                <div className='standard-body'>
                                    <div className='the-comparison-content'>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={checked}
                                                    onChange={handleChange}
                                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                                    color='primary'
                                                />
                                            }
                                            label="Print Logo"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='button-container' >
                            <QuoteButton type={'flip'} color={color.hex} thickness={thickness} height={height} checked={checked} dia={coreDia.value} />
                            <TheCustomButton type={'flip'}/>                
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}