import React from 'react';
import '../product/product.scss';
import Helmet from 'reactor/components/helmet';
import TypesCategory from './typesCategorize';
import ProductsIntro from './productsIntro';
import QuoteForm from './quote-form';

export default function Product(props) {

    return (
        <>
            <Helmet title="Our Products" description="my desc" bodyClass="product-page" id="test" />
            <div className="the-products-container">
                <ProductsIntro />
            </div>
            <div className='quote-part'>
                <TypesCategory name={props.params.name} />
            </div>
            <div className='big-quote-form'>
                <QuoteForm />
            </div>
        </>
    )
}