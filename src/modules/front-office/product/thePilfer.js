import React from 'react';
import Pilfer from '../../../assets/images/PILFER2.png'
import QuoteButton from './quoteButton';
import TheCustomButton from './customButton';
import { SketchPicker } from 'react-color';
import { Button, Checkbox, FormControlLabel, styled } from '@material-ui/core';
import cls from 'clsx';

const heights = [
    14.5,
    15,
    15.5,
    16,
    17,
    18,
    19,
];

const thicknessList = [
    0.18,
    0.19,
    0.20,
    0.21,
    0.22,
    0.23,
    0.24,
    0.25,
]

const coreDiaList = [
    {
        value: 22,
        minHeight: 14.5,
        maxHeight: 16,
    },
    {
        value: 25,
        minHeight: 15,
        maxHeight: 19,
    },
    {
        value: 28,
        minHeight: 17,
        maxHeight: 19,
    },
]

const CustomButton = styled(Button)({
    outline: 'none',
    textTransform: 'lowercase',
    border: ' none',
})



export default function ThePilfer() {

    const [checked, setChecked] = React.useState(false);
    const handleChange = (event) => {
        setChecked(event.target.checked);
    };
    const [color, setColor] = React.useState({
        hex: null
    });
    const [height, setHeight] = React.useState('');
    const [coreDia, setCoreDIa] = React.useState({
        minHeight: 0,
        maxHeight: 0,
    }); // initial no selection
    const [thickness, setThickness] = React.useState('');

    const isSelectedDia = theDia => cls({
        'normalSelection': true,
        activeSelection: coreDia === theDia
    })

    const isSelectedHeight = theHeight => cls({
        'normalSelection': true,
        activeSelection: height === theHeight
    })

    const isSelectedThickness = theThickness => cls({
        'normalSelection': true,
        activeSelection: thickness === theThickness
    })


    return (
        <>
            <div className="the-main-container">
                <div className="the-type-description">
                    <p>Inner, ​coating by high quality of material, suitable with FDA & food base. Outer, according requirements of client with more than 10 colors of logo or plain gold, suitable with FDA & food base. Over print, high quality of varnish Scratch resistant.</p>
                </div>
                <div className='quote-content'>
                    <div className='quote-content-left-part'>
                        <img src={Pilfer} className='quote-image' alt='quote ' />
                    </div>
                    <div className='quote-content-right-part'>
                        <div className='big-right-part'>
                            <div className='standard'>
                                <div className='standard-header'>
                                    <h1>Dimensions</h1>
                                    <div className='small-types-comparison'>
                                        <p>core dia</p>
                                        <p>H(height)</p>
                                    </div>
                                </div>
                                <div className='standard-body'>
                                    <div className='the-comparison'>
                                        <div className='the-comparison-content'>
                                            {coreDiaList.map(diameter => (
                                                <CustomButton className={isSelectedDia(diameter)} onClick={() => setCoreDIa(diameter)}>{diameter.value} mm</CustomButton>
                                            ))}
                                        </div>
                                        <div className="quote-vl"></div>
                                        <div className="the-comparison-content">
                                            {heights.map(height => (
                                                <CustomButton className={isSelectedHeight(height)} onClick={() => setHeight(height)} type="button" disabled={height < coreDia.minHeight || height > coreDia.maxHeight}>{height} mm</CustomButton>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='standard'>
                                <div className='standard-header'>
                                    <h1>Thickness</h1>
                                </div>
                                <div className='standard-body'>
                                    <div className='the-comparison-content'>
                                        {thicknessList.map(thick => (
                                            <CustomButton className={isSelectedThickness(thick)} onClick={() => setThickness(thick)} type="button">{thick} mm</CustomButton>
                                        ))}
                                    </div>
                                </div>
                            </div>
                            <div className='standard'>
                                <div className='standard-header'>
                                    <h1>Color Palette</h1>
                                </div>
                                <div className='standard-body'>
                                    <div className='the-comparison-content'>
                                        <SketchPicker className='colored' color={color} onChange={updatedColor => setColor(updatedColor)} />
                                    </div>
                                </div>
                            </div>
                            <div className='standard'>
                                <div className='standard-header'>
                                    <h1>Logo Availability</h1>
                                </div>
                                <div className='standard-body'>
                                    <div className='the-comparison-content'>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={checked}
                                                    onChange={handleChange}
                                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                                    color='primary'
                                                />
                                            }
                                            label="Print Logo"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='button-container' >
                            <QuoteButton type={'pilfer'} color={color.hex} thickness={thickness} height={height} checked={checked} dia={coreDia.value} />
                            <TheCustomButton type={'pilfer'} />                
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}