import React from 'react';
import { Button, styled } from '@material-ui/core';
import QuoteForm from './quote-form';
import Form from 'reactor/form/components/form';
import Modal from 'reactor/components/modal';

const RequestButton = styled(Button)({
    cursor: 'pointer',
    backgroundColor: '#1F4B66',
    borderRadius: '25px',
})

export default function QuoteButton(props) {

    const [isOpen, setIsOpen] = React.useState(false);
    const toggleForm = () => {
        setIsOpen(!isOpen);
    }

    return (
        <Form>
            <div className='quote-btn'>
                <RequestButton variant="contained" color="primary" onClick={toggleForm}>
                    Request Quote
                </RequestButton>
            </div>
            <Modal
                open={isOpen}
                size="md"
                backdrop
                esc
                onClose={() => setIsOpen(false)}
                children={<QuoteForm {...props} closePopup={() => setIsOpen(false)} />}
            />
        </Form>
    )
}

