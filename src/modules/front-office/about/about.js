import React from 'react';
import '../about/about.scss';
// import factory from '../../../assets/images/factory.png';
import video1 from '../../../assets/videos/video2.mp4';
import video2 from '../../../assets/videos/video3.mp4';
// import mainFactory from '../../../assets/images/mainFactory.jpg';
import factory1 from '../../../assets/images/factory1.JPG';
import factory2 from '../../../assets/images/factory2.JPG';
import factory3 from '../../../assets/images/factory3.jpg';
import factory4 from '../../../assets/images/factory4.png';
import factory5 from '../../../assets/images/factory5.png';
import factory6 from '../../../assets/images/factory6.JPG';
import Helmet from 'reactor/components/helmet';
export default function About() {

    return (
        <>
            <Helmet title="About Us" description="my desc" bodyClass="about-page" id="test" />
            <div className='aboutUs-container'>
                <div className="about-header">
                    <h1 className="aboutUs-header-font">ABOUT US</h1>
                    <div className="aboutUs-description">
                        <div>
                            <h1>OUR COMPANY</h1>
                        </div>
                        <div>
                            <p>
                                We are best manufactures of Closure caps & Pilfer Proof & Flip-Off Caps For pharmaceuticals and food products in Egypt and Africa. All our products are manufactured to the highest quality standards, and our company has been awarded ISO:9001, ISO:14001,ISO:22000, ISO:45001.
                            </p>
                        </div>
                    </div>
                </div>
                <div className="our-factories">
                    <div className="our-factories-header">
                        <h1>OUR FACTORIES</h1>
                    </div>
                    <div className="our-factories-container">

                        <div className="about-card">
                            {/* <img src={factory} alt='factory1' className='factory-image-style' /> */}
                            {/* <video src={video1} className='factory-image-style'/> */}
                            <video src={video2} className='videos' controls="controls" autoplay="false" />

                            <div className='small-images'>
                                <img src={factory1} alt='factory1' className='small-image-style' />
                                <img src={factory2} alt='factory1' className='small-image-style' />
                                <img src={factory5} alt='factory1' className='small-image-style' />
                            </div>
                            <div className="about-card-description">
                                <h1>Pilfer Proof Factory</h1>
                                <p>​We are best manufactures of Closure caps & Pilfer Proof For pharmaceuticals and food products in Egypt and Africa. All our products are manufactured to the highest quality standards, and our company has been awarded ISO:9001, ISO:14001,ISO:22000, ISO:45001.</p>
                                <p>​We are best manufactures of Closure caps & Pilfer Proof For pharmaceuticals and food products in Egypt and Africa. All our products are manufactured to the highest quality standards, and our company has been awarded ISO:9001, ISO:14001,ISO:22000, ISO:45001.</p>
                            </div>
                        </div>
                        <div className="about-card">
                            {/* <img src={mainFactory} alt='factory1' className='factory-image-style' /> */}
                            <video src={video1} className='videos' controls="controls" autoplay="false" />

                            <div className='small-images'>
                                <img src={factory4} alt='factory1' className='small-image-style' />
                                <img src={factory3} alt='factory1' className='small-image-style' />
                                <img src={factory6} alt='factory1' className='small-image-style' />
                            </div>
                            <div className="about-card-description">
                                <h1>Flip Off Factory</h1>
                                <p>​We are best manufactures of Flip-Off & Aluminum Seals Caps For pharmaceuticals products in Egypt and Africa. All our products are manufactured to the highest quality standards, and our company has been awarded ISO:9001, ISO:14001,ISO:22000, ISO:45001.</p>
                                <p>​We are best manufactures of Flip-Off & Aluminum Seals Caps For pharmaceuticals products in Egypt and Africa. All our products are manufactured to the highest quality standards, and our company has been awarded ISO:9001, ISO:14001,ISO:22000, ISO:45001.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}