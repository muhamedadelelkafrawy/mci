import router from 'reactor/router';
import Layout from 'shared/components/Layout';
import LayoutWithNewHeader from 'shared/components/LayoutWithNewHeader';


export default function frontRoutes(path, component) {
    router.group({
        layout: Layout,
        routes: [{
            path,
            component,
        }],
    });
}


export function withNewHeader(path, component) {
    router.group({
        layout: LayoutWithNewHeader,
        routes: [{
            path,
            component,
        }],
    });
}
