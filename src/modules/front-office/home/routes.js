import frontRoutes, { withNewHeader } from './helpers/frontRoutes';
import Home from '../home/components/Home';
import About from '../about/about';
import Contact from '../contact/contact';
import TheQuality from '../quality/quality';
import Product from '../product/product';

frontRoutes('/', Home);

withNewHeader('/about-us', About); 
withNewHeader('/contact-us', Contact); 
withNewHeader('/quality', TheQuality); 
withNewHeader('/product/:name', Product); // name here is the paramter which i send
withNewHeader('/product', Product); 