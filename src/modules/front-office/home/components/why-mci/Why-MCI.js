import React from 'react';
import './Why-MCI.scss';
import why1 from '../../../../../assets/images/try2.jpeg'
import why2 from '../../../../../assets/images/7.jpg'
import why3 from '../../../../../assets/images/3.jpg'
import why4 from '../../../../../assets/images/try1.jpeg'

export default function WhyMCI() {
    return (
        <>
            <div className="why-container">
                <div className="why-header">
                    <h1>WHY MCI ? </h1>
                </div>
                <div className="big-container">
                    <div className="left-container">
                        <div className="why-card-container">
                            <div className="why-card">
                                <div className="why-number">
                                    <div className="numberCircle blue">1</div>
                                </div>
                                <div className="why-content">
                                    <div className="why-content-header">
                                        <h1>High Quality</h1>
                                    </div>
                                    <div className="why-content-description">
                                        <p>MCI is certified with ISO:9001, ISO:14001, ISO:22000, ISO:45001.
                                        And we are committed to provide the products at their best quality and achieving all the requirements of the Egyptian and international standard specifications.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="why-card-container">
                            <div className="why-card">
                                <div className="why-number">
                                    <div className="numberCircle orange">2</div>
                                </div>
                                <div className="why-content">
                                    <div className="why-content-header">
                                        <h1>Specialization</h1>
                                    </div>
                                    <div className="why-content-description">
                                        <p>We have more than 20 years of experience and specialization in manufacturing Closure caps & Pilfer proof & Flip-off caps, with proper infrastructure supported and the automatic production scrupulous line.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="why-card-container">
                            <div className="why-card">
                                <div className="why-number">
                                    <div className="numberCircle blue">3</div>
                                </div>
                                <div className="why-content">
                                    <div className="why-content-header">
                                        <h1>Commitment After Sales</h1>
                                    </div>
                                    <div className="why-content-description">
                                        <p>For keeping the best after-sales service commitment, our professional service staff will make clear the product information, receive any inquiries or complaints regarding products, and giving you the corresponding solutions.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="why-card-container">
                            <div className="why-card">
                                <div className="why-number">
                                    <div className="numberCircle orange">4</div>
                                </div>
                                <div className="why-content">
                                    <div className="why-content-header">
                                        <h1>Fast Supply</h1>
                                    </div>
                                    <div className="why-content-description">
                                        <p>One of our most important features in MCI, our safe and fast supply of all desired shipping quantities, to always provide a high-quality service for our customers
</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="right-container">
                        <img className="why-mci-image" src={why1} alt="first-one" />
                        <img className="why-mci-image" src={why2} alt="second-one" />
                        <img className="why-mci-image" src={why3} alt="second-one" />
                        <img className="why-mci-image" src={why4} alt="second-one" />
                    </div>
                </div>
            </div>
        </>
    )
}