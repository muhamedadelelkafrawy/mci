import React from 'react';
import '../our-clients/Our-Clients.scss';
import logo1 from 'assets/logos/logo1.png';
import logo2 from 'assets/logos/logo2.png';
import logo3 from 'assets/logos/logo3.png';
import logo4 from 'assets/logos/logo4.png';
import logo5 from 'assets/logos/logo5.png';
import logo6 from 'assets/logos/logo6.png';
import logo7 from 'assets/logos/logo7.png';
import logo9 from 'assets/logos/logo9.png';
import logo10 from 'assets/logos/logo10.png';
import logo11 from 'assets/logos/logo11.png';
import logo12 from 'assets/logos/logo12.png';
import logo13 from 'assets/logos/logo13.png';
import logo14 from 'assets/logos/logo14.png';
import logo16 from 'assets/logos/logo16.png';
import logo17 from 'assets/logos/logo17.png';
import logo18 from 'assets/logos/logo18.png';
import logo19 from 'assets/logos/logo19.png';
import logo20 from 'assets/logos/logo20.png';
import logo21 from 'assets/logos/logo21.png';
import logo22 from 'assets/logos/logo22.png';
import logo23 from 'assets/logos/logo23.png';
import logo24 from 'assets/logos/logo24.png';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.min.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';

export default function OurClients() {

    const options = {
        loop: true,
        margin: 10,
        dots:false,
        nav: true,
        navText: ['<span class="fas fa-chevron-left fa-2x"></span>','<span class="fas fa-chevron-right fa-2x"></span>'],
        responsive: {
            0: {
                items: 3
            },
            600: {
                items: 4
            },
            1000: {
                items: 4
            }
        },
    };
    return (
        <>
            <div id="Clients" className="clients-container">
                <div className='clients-header'>
                    <h1 className="clients-header-font">OUR CLIENTS</h1>
                </div>
                
                <OwlCarousel
                    className="owl-theme owl-BG"
                    {...options}
                >
                    <div className='item2'>
                        <img src={logo1} alt='logo1' />
                    </div>
                    <div className='item2'>
                        <img src={logo2} alt='logo2' />
                    </div>
                    <div className='item2'>
                        <img src={logo3} alt='logo3' />
                    </div>
                    <div className='item2'>
                        <img src={logo4} alt='logo4' />
                    </div>
                    <div className='item2'>
                        <img src={logo5} alt='logo5' />
                    </div>
                    <div className='item2'>
                        <img src={logo6} alt='logo6' />
                    </div>
                    <div className='item2'>
                        <img src={logo7} alt='logo7' />
                    </div>
                    
                    <div className='item2'>
                        <img src={logo9} alt='logo9' />
                    </div>
                    <div className='item2'>
                        <img src={logo10} alt='logo10' />
                    </div>
                    <div className='item2'>
                        <img src={logo11} alt='logo11' />
                    </div>
                    <div className='item2'>
                        <img src={logo12} alt='logo12' />
                    </div>
                    <div className='item2'>
                        <img src={logo13} alt='logo13' />
                    </div>
                    <div className='item2'>
                        <img src={logo14} alt='logo14' />
                    </div>
                    
                    <div className='item2'>
                        <img src={logo16} alt='logo16' />
                    </div>
                    <div className='item2'>
                        <img src={logo17} alt='logo17' />
                    </div>
                    <div className='item2'>
                        <img src={logo18} alt='logo18' />
                    </div>
                    <div className='item2'>
                        <img src={logo19} alt='logo19' />
                    </div>
                    <div className='item2'>
                        <img src={logo20} alt='logo20' />
                    </div>
                    <div className='item2'>
                        <img src={logo21} alt='logo21' />
                    </div>
                    <div className='item2'>
                        <img src={logo22} alt='logo22' />
                    </div>
                    <div className='item2'>
                        <img src={logo23} alt='logo22' />
                    </div>
                    <div className='item2'>
                        <img src={logo24} alt='logo22' />
                    </div>
                   
                </OwlCarousel>
                
            </div>
        </>
    )
}