import React from 'react';
import '../our-clients-from/our-clients-from.scss';
import ClientsMap from 'assets/images/Group651.png';

export default function OurClientsFrom() {

    return (
        <>
            <div className="clients-from-container">
                <div className="clients-from-header ">
                    <h1>OUR CLIENTS FROM</h1>
                </div>
                <div className="clients-map-container">
                    <img className="clients-map" src={ClientsMap} alt="map" />
                </div>
            </div>
        </>
    )
}