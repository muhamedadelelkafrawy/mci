import { Button, makeStyles, TextField } from '@material-ui/core';
import Axios from 'axios';
import React from 'react';
import Form from 'reactor/form/components/form';
import '../contact-us/Contact-Us.scss';
import '../contact-us/contactus.php';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

export default function ContactUs() {
    const classes = useStyles();

    const [isSuccess, setSuccess] = React.useState();

    const formRequest = e => {
        Axios.post('./contactus.php', new FormData(e.target)).then(response => {
            setSuccess(true);
        })
    };

    if (isSuccess) {
        return (
            <h3 className="successMessage">Your Request Is Submitted Successfully </h3>
        )
    }


    return (
        <>
            <div id='Contact' className="contact-container">
                <div className="contact-header">
                    <h1 className="contact-header-font">Contact Us</h1>
                </div>
                <Form onSubmit={formRequest}>
                    <div className="contact-form">
                        <div className={classes.root} noValidate autoComplete="off">
                            <TextField name="name" className='input-field' label="Full Name" variant="outlined" />
                            <TextField name="email" className='input-field' label="ُEmail Address" variant="outlined" />
                        </div>
                        <div className={classes.root} noValidate autoComplete="off">
                            <TextField name="phoneNumber" className='input-field' type='number' label="Phone Number" variant="outlined" />
                            <TextField name="subject" className='input-field' label="ُSubject" variant="outlined" />
                        </div>
                        <div className={classes.root} noValidate autoComplete="off">
                            <TextField name="message" rows={10} multiline={true} className='input-field-long' label="Your Message" variant="outlined" />
                        </div>
                        <div className={classes.root} noValidate autoComplete="off">
                            <Button type="submit" variant="contained" className='submit-btn'>
                                Submit
                         </Button>
                        </div>
                    </div>
                    <div id="server-results">
                        {/* For server results */}
                    </div>
                    <div className='contact-header'>
                        <h1 className="contact-header-font">Our Location</h1>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3447.297292251259!2d31.734854715119425!3d30.22859758181558!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzDCsDEzJzQzLjAiTiAzMcKwNDQnMTMuNCJF!5e0!3m2!1sen!2seg!4v1607860269157!5m2!1sen!2seg" title="title" className="google-map" frameBorder="0"  allowFullScreen=""  tabIndex="0"></iframe>
                    </div>
                </Form>
            </div>
        </>
    )

}