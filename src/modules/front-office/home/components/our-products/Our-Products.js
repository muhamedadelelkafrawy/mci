import React from 'react';
import '../our-products/Our-Products.scss';
import Pilfer from 'assets/images/PILFER.png';
import Flip from 'assets/images/FLIP.png';
import Aluminum from 'assets/images/alu seals.png';
import Plastic from 'assets/images/plasticCaps.jpg';
import ComingSoon from 'assets/images/com-soon.png';
import { Link } from 'react-router-dom';

export default function OurProducts() {

    return (
        <>

            <div className="products-container">
                <div className="centered">
                    <h1 className="products-header"> OUR PRODUCTS </h1>
                </div>
                <div className='products-images'>
                    <div className='card'>
                        <img src={Pilfer} alt='product1' className='product-image-style' />
                        <div className='card-description'>
                            <h3>Pilfer Proof Caps</h3>
                            <p><strong>Inner,</strong> ​coating by high quality of material, suitable with FDA & food base. Outer, according requirements of client with more than 10 colors of logo or plain gold, suitable with FDA & food base.
                                <strong> Over print, </strong>high quality of varnish Scratch resistant.</p>
                            <Link to="/product/pilfer">Read More >></Link>
                        </div>
                    </div>
                    <div className='card'>
                        <img src={Flip} alt='product1' className='product-image-style' />
                        <div className='card-description'>
                            <h3>Flip-Off Caps</h3>
                            <p><strong>FDA</strong> coating inside and outside , gold color or silver , with logo printing or  Carved injection with plastic part (as request), <strong>Suitable</strong> for all types of pharmaceutical vials, and suitable for all sterilization operations by steam, gamma, and autoclave.</p>
                            <Link to="/product/flip">Read More >></Link>
                        </div>
                    </div>
                    <div className='card'>
                        <img src={Aluminum} alt='product1' className='product-image-style' />
                        <div className='card-description'>
                            <h3>Aluminum Seals</h3>
                            <p><strong>FDA</strong> coating inside and outside , gold color or silver , with logo curved (as request),<strong> Suitable</strong>  for all types of pharmaceutical vials, and suitable for all sterilization operations by steam, gamma, and autoclave.</p>
                            <Link to="/product/alumunium">Read More >></Link>
                        </div>
                    </div>
                    <div className='card'>
                        <img src={Plastic} alt='product1' className='product-image-style' />
                        <div className='com-soon-div'>
                            <img src={ComingSoon} alt='coming-soon' className='coming-soon-style' />
                        </div>
                        <div className='card-description'>
                            <h3>Plastic Caps</h3>
                            <p> <strong>High</strong> quality of raw material polypropylene or as your request raw material, suitable with FDA & food base. <strong>Outer,</strong> according requirements of client with more than 10 colors of logo, suitable with FDA & food base.</p>
                            {/* <Link to="/product/plastic">Read More >></Link> */}
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}