import { Button, createMuiTheme, styled, ThemeProvider } from '@material-ui/core';
import React from 'react';
import '../about-us/About-Us.scss';
import Link from 'reactor/components/link';

// const useStyles = makeStyles((theme) => ({
//     root: {
//         '& > *': {
//             margin: theme.spacing(1),
//             zIndex: '1',
//         },
//     },
// }));

const theme = createMuiTheme({
    palette: {
        primary: {
            light: 'white',
            main: '#EA8E4A',
            dark: '#1F4B66',
            contrastText: '#fff',
        },
    },
});

const StyledLink = styled(Link)({
    textTransform: 'none',
    color:'white',
})


export default function AboutUs() {
    // const classes = useStyles();

    return (
        <>
            <div className="about-container">
                <div className="about-header">
                    <h1 className="about-header-font">ABOUT US</h1>
                    <div className='about-description'>
                        <p>
                            We are best manufactures of Closure caps & Pilfer Proof & Flip-Off Caps For pharmaceuticals and food products in Egypt and Africa.
                    </p>
                        <br />
                        <br />
                        <br />
                        <br />
                        <p>
                            All our products are manufactured to the highest quality standards, and our company has been awarded ISO:9001 ,ISO:14001 ,ISO:22000 ,ISO:45001.
                    </p>
                    </div>
                    <ThemeProvider theme={theme}>
                        <Button variant="contained" color="primary" className='test-btn'>
                            <StyledLink to="/about-us">
                                 Read More
                            </StyledLink>
                        </Button>
                    </ThemeProvider>
                </div>
            </div>
        </>
    )
}