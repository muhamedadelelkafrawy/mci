import React from 'react';
import '../quality/Quality.scss';
import cer1 from '../../../../../assets/images/Cer1.jpg';
import cer2 from '../../../../../assets/images/Cer2.jpg';
import cer3 from '../../../../../assets/images/Cer3.jpg';
import cer4 from '../../../../../assets/images/Cer4.jpg';
export default function Quality() {

    return (
        <>
            <div className="quality-container">
                <div className='quality-header'>
                    <h1 className="quality-header-font">QUALITY CERTIFICATES</h1>
                </div>
                <div className='quality-images'>
                        <img src={cer1} className='quality' alt='quality1' />
                        <img src={cer2} className='quality' alt='quality2' />
                        <img src={cer3} className='quality' alt='quality3' />
                        <img src={cer4} className='quality' alt='quality4' />
                </div>
            </div>
        </>
    )
}