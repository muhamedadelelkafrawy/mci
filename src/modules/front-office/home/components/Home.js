import React from 'react';
import MainSlider from 'shared/components/Header/mainSlider';
import AboutUs from './about-us/About-Us';
// import ContactUs from './contact-us/Contact-Us';
import OurClientsFrom from './our-clients-from/our-clients-from';
import OurClients from './our-clients/Our-Clients';
import OurProducts from './our-products/Our-Products';
import Quality from './quality/Quality';
import WhyMCI from './why-mci/Why-MCI';

export default function Home() {
    return (
        <>
            <MainSlider/>
            <OurProducts />
            <AboutUs />
            <WhyMCI />
            <OurClients />
            <OurClientsFrom />
            <Quality />
            {/* <ContactUs /> */}
        </>
    );
} 