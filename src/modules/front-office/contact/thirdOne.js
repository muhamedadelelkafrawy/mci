import React from 'react';

export default function ThirdOne() {
    return (
        <div className="the-locations-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3447.297292251259!2d31.734854715119425!3d30.22859758181558!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzDCsDEzJzQzLjAiTiAzMcKwNDQnMTMuNCJF!5e0!3m2!1sen!2seg!4v1607860269157!5m2!1sen!2seg" title="title" className="location-on-map" frameBorder="0" allowFullScreen="" tabIndex="0"></iframe>
        </div>
    )
}