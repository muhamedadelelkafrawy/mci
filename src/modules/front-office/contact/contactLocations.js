import React from 'react';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import FirstOne from './firstOne';
import SecondOne from './secondOne';
import ThirdOne from './thirdOne';
import cls from 'clsx';

const LocationsList = {
    firstLocation: FirstOne,
    secondLocation: SecondOne,
    thirdLocation: ThirdOne,
}

export default function ContactLocations() {
    const [status, setStatus] = React.useState('firstLocation');
    const Locations = status ? LocationsList[status] : React.Fragment;


    // function cls(object) {
    //     let classes = '';

    //     for (let key in object) {
    //         let booleanValue = object[key];

    //         if (booleanValue === false) continue;


    //         classes += key + '  ';
    //     }

    //     return classes;
    // }

    // function isSelectedLocation(location) {
    //     return cls({
    //         activeLocation: location === status
    //     });        
    // }
    // for two classes
    // function isSelectedLocation(location) {
    //     return cls({
    //         locationStyle: true,
    //         activeLocation: location === status
    //     });        
    // }

    // const isSelectedLocation = location => cls({
    //     locationStyle: true,
    //     activeLocation: location === status
    // });
    const isSelectedLocation = location => cls({
        activeLocation: location === status
    });

    return (
        <>
            <div className="big-locations">
                <div className="contact-information-header">
                    <h1 className="contact-header-font">OUR LOCATIONS</h1>
                </div>
                <div className="our-locations">
                    <div className="the-locations">
                        <div className="first-part">
                            <h1 onClick={() => setStatus('firstLocation')} className={isSelectedLocation('firstLocation')} >Pilfer Proof Factory:</h1>
                            <div className='same-row'>
                                <LocationOnIcon style={{ color: '#EA8E4A' }} />
                                <p>first, BLOCK 93, (A6) Area,10th Of Ramadan City,Al-Sharika, Egypt.</p>
                            </div>
                        </div>
                        <div className="first-part">
                            <h1 onClick={() => setStatus('secondLocation')} className={isSelectedLocation('secondLocation')} >Flip Off Factory:</h1>
                            <div className='same-row'>
                                <LocationOnIcon style={{ color: '#EA8E4A' }} />
                                <p>first, Block 94, (A6) Area,10th Of Ramadan City,Al-Sharika, Egypt.</p>
                            </div>
                        </div>
                        {/* <div className="first-part">
                            <h1 onClick={() => setStatus('thirdLocation')} className={isSelectedLocation('thirdLocation')} >The Head Office:</h1>
                            <div className='same-row'>
                                <LocationOnIcon style={{ color: '#EA8E4A' }} />
                                <p>first, BLOCK 93, Block 94, (A6) Area,10th Of Ramadan City,Al-Sharika, Egypt.</p>
                            </div>
                        </div> */}
                    </div>
                    <Locations />
                </div>
            </div>

        </>
    )
}
