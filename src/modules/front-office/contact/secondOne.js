import React from 'react';

export default function SecondOne() {
    return (
        <div className="the-locations-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m21!1m12!1m3!1d110483.90278208433!2d31.220202270027148!3d30.076368884370254!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m6!3e6!4m0!4m3!3m2!1d30.05097!2d31.243979999999997!5e0!3m2!1sen!2seg!4v1608024624591!5m2!1sen!2seg" title="title" className="location-on-map" frameBorder="0" allowFullScreen="" tabIndex="0"></iframe>
        </div>
    )
}