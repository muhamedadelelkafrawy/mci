import React from 'react';
import { Button, TextField, makeStyles } from '@material-ui/core';
import Axios from 'axios';
import Form from 'reactor/form/components/form';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

export default function ContactForm() {

    const classes = useStyles();
    const [isSuccess, setSuccess] = React.useState();

    const contactFormSubmit = e => {
        Axios.post('./contactus.php', new FormData(e.target)).then(response => {
            setSuccess(true);
        })
    };

    if (isSuccess) {
        return (
            <h3 className="successMessage">Your Request Is Submitted Successfully </h3>
        )
    }
    return (
        <>
            <Form onSubmit={contactFormSubmit}>
                <div className="contact-form">
                    <h1 className="contact-header-font">CONTACT US</h1>
                    <div className={classes.root} noValidate autoComplete="off">
                        <TextField name="name" className='input-field' label="Full Name" variant="outlined" />
                        <TextField name="email" className='input-field' label="Email Address" variant="outlined" />
                    </div>
                    <div className={classes.root} noValidate autoComplete="off">
                        <TextField name="phoneNumber" className='input-field' type='number' label="Phone Number" variant="outlined" />
                        <TextField name="subject" className='input-field' label="Subject" variant="outlined" />
                    </div>
                    <div className={classes.root} noValidate autoComplete="off">
                        <TextField name="message" rows={10} multiline={true} className='input-field-long' label="Your Message" variant="outlined" />
                    </div>
                    <div className={classes.root} noValidate autoComplete="off">
                        <Button type="submit" variant="contained" className='submit-btn'>
                            Submit
                    </Button>
                    </div>
                </div>
            </Form>
        </>
    )
}
