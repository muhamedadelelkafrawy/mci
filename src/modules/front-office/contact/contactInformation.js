import React from 'react';
import MailIcon from '@material-ui/icons/Mail';
import CallIcon from '@material-ui/icons/Call';

export default function ContactInformation() {

    return (
        <>
            <div className='big-contact-info'>
                <div className='contact-information-header'>
                    <h1 className='contact-header-font'>CONTACT INFORMATION</h1>
                </div>
                <div className='contact-information'>
                    <div className='the-company'>
                        <div className='second-part'>
                            <h1>Email Address:</h1>
                            <div className='same-row'>
                                <MailIcon style={{ color: '#EA8E4A' }} />
                                <p>info@mci.com</p>
                            </div>
                            <div className='same-row'>
                                <MailIcon style={{ color: '#EA8E4A' }} />
                                <p>info@mci.com</p>
                            </div>
                        </div>
                    </div>
                    <div className='the-factory'>
                        <div className='second-part'>
                            <h1>Phone Numbers:</h1>
                            <div className='same-row'>
                                <CallIcon style={{ color: '#EA8E4A' }} />
                                <p>+20 1000092622</p>
                            </div>
                            <div className='same-row'>
                                <CallIcon style={{ color: '#EA8E4A' }} />
                                <p>+20 1024607316</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
