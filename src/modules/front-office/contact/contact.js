import React from 'react';
import '../contact/contact.scss';
import Helmet from 'reactor/components/helmet';
import ContactForm from './contactForm';
import ContactInformation from './contactInformation';
import ContactLocations from './contactLocations';

export default function Contact() {

    return (
        <>
            <Helmet title="Contact Us" description="my desc" bodyClass="contact-page" id="test" />
            <div className='contact-container'>
                <ContactForm />
                <ContactInformation/>
                <ContactLocations/>
            </div>
        </>
    )
}