import React from 'react';
import '../quality/quality.scss';
import Helmet from 'reactor/components/helmet';
import FirstQuality from '../../../assets/images/try2.jpeg'
import SecondQuality from '../../../assets/images/7.jpg'
import ThirstQuality from '../../../assets/images/try3.jpeg'
import FourthQuality from '../../../assets/images/try4.jpeg'
import Quality from '../home/components/quality/Quality';
export default function TheQuality() {

    return (
        <>
            <Helmet title="Quality" description="my desc" bodyClass="quality-page" id="test" />
            <div className='the-quality-container'>
                <div className='the-quality-header'>
                    <h1>WHY MCI ?</h1>
                </div>
                <div className='big-quality'>
                    <div className='the-quality-part'>
                        <div className='the-quality-left-part'>
                            <div className='the-quality-part-number'>
                                <div className="number-circle blue">1</div>
                            </div>
                            <div className='the-quality-left-part-description'>
                                <div className='the-quality-left-part-description-header'>
                                    <h1>High Quality</h1>
                                </div>
                                <div className='the-quality-left-part-description-content'>
                                    <p>MCI is certified with ISO:9001, ISO:14001, ISO:22000, ISO:45001.
                                    And we are committed to provide the products at their best quality and achieving all the requirements of the Egyptian and international standard specifications.
                                        </p>
                                </div>
                            </div>
                        </div>
                        <div className='the-quality-right-part'>
                            <img src={FirstQuality} className='the-quality-pictures' alt='quality ' />
                        </div>
                    </div>
                </div>
                <div className='big-quality'>
                    <div className='reverse-the-quality-part'>
                        <div className='the-quality-left-part'>
                            <div className='reverse-the-quality-left-part-description'>
                                <div className='reverse-the-quality-left-part-description-header'>
                                    <h1>Specialization</h1>
                                </div>
                                <div className='reverse-the-quality-left-part-description-content'>
                                    <p>We have more than 20 years of experience and specialization in manufacturing Closure caps & Pilfer proof & Flip-off caps, with proper infrastructure supported and the automatic production scrupulous line.
                                        </p>
                                </div>
                            </div>
                            <div className='reverse-the-quality-part-number'>
                                <div className="number-circle orange">2</div>
                            </div>
                        </div>
                        <div className='reverse-the-quality-right-part'>
                            <img src={SecondQuality} className='the-quality-pictures' alt='quality ' />
                        </div>
                    </div>
                </div>
                <div className='big-quality'>
                    <div className='the-quality-part'>
                        <div className='the-quality-left-part'>
                            <div className='the-quality-part-number'>
                                <div className="number-circle blue">3</div>
                            </div>
                            <div className='the-quality-left-part-description'>
                                <div className='the-quality-left-part-description-header'>
                                    <h1>Commitment after sales</h1>
                                </div>
                                <div className='the-quality-left-part-description-content'>
                                    <p>For keeping the best after-sales service commitment, our professional service staff will make clear the product information, receive any inquiries or complaints regarding products, and giving you the corresponding solutions.
                                        </p>
                                </div>
                            </div>
                        </div>
                        <div className='the-quality-right-part'>
                            <img src={ThirstQuality} className='the-quality-pictures' alt='quality ' />
                        </div>
                    </div>
                </div>
                <div className='big-quality'>
                    <div className='reverse-the-quality-part'>
                        <div className='the-quality-left-part'>
                            <div className='reverse-the-quality-left-part-description'>
                                <div className='reverse-the-quality-left-part-description-header'>
                                    <h1>Fast Supply</h1>
                                </div>
                                <div className='reverse-the-quality-left-part-description-content'>
                                    <p>One of our most important features in MCI, our safe and fast supply of all desired shipping quantities, to always provide a high-quality service for our customers
</p>
                                </div>
                            </div>
                            <div className='reverse-the-quality-part-number'>
                                <div className="number-circle orange">4</div>
                            </div>
                        </div>
                        <div className='reverse-the-quality-right-part'>
                            <img src={FourthQuality} className='the-quality-pictures' alt='quality ' />
                        </div>
                    </div>
                </div>
                <Quality />
            </div>
        </>
    )
}