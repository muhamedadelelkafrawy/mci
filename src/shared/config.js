import config from 'reactor/config';

const inDevelopmentMode = process.env.NODE_ENV === 'development';

config.set({
    // Services Configurations
    // A.K.A Endpoints
    basePath: inDevelopmentMode ? '/' : '/mci/', // for production
    endpoint: {
        baseUrl: 'https://apps.crafted-internet.com/mci',
        apiKey: 'PAQWEQCZXDGSDKPW912EDWCSXP213EDAYFEKRIANOFEKRYNO1',
    },
    locales: {
        en: {
            direction: 'ltr',
        },
    },
    form: {
        input: {
            variant: 'standard'
        }
    }
});