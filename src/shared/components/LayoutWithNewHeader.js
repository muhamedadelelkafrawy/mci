import React from 'react';
import MainLayout from 'reactor/layout/components/layout';
import Footer from './Footer/Footer';
import NewHeader from './Header/NewHeader';

export default function LayoutWithNewHeader(props) {
    return (
        <MainLayout>
            <NewHeader />
            {props.children}
            <Footer />
        </MainLayout>
    )
} 