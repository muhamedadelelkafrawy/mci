import React from 'react';
import '../Footer/Footer.scss';
import FacebookIcon from '@material-ui/icons/Facebook';
import MailIcon from '@material-ui/icons/Mail';
import TwitterIcon from '@material-ui/icons/Twitter';
import Link from 'reactor/components/link';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import CallIcon from '@material-ui/icons/Call';
import Is from '@flk/supportive-is';
import MobileFooter from './mobileFooter';

export default function Footer() {

    if (Is.mobile.any() && !Is.mobile.ipad()) return <MobileFooter />

    return (
        <>
            <div className="footer-container">
                <div className="the-partition">
                    <Link Link className="footer-links" to='/'>Home</Link>
                    <Link className="footer-links" to='/about-us'>About Us</Link>
                    <Link className="footer-links" to='/product'>Products</Link>
                    <Link className="footer-links" to='/quality'>Quality</Link>
                    <Link className="footer-links" to='/contact-us'>Contact Us</Link>
                </div>
                <div className="vl"></div>
                <div className="the-partition">
                    <div className="the-row">
                        <LocationOnIcon style={{ color: '#EA8E4A' }} />
                        <p>first, BLOCK 93, (A6) <br/> Area,10th Of Ramadan<br/> City,Al-Sharika, Egypt.</p>
                    </div>
                    <div className="the-row">
                        <CallIcon style={{ color: '#EA8E4A' }} />
                        <p>+20 1000092622</p>
                    </div>
                    <div className="the-row">
                        <CallIcon style={{ color: '#EA8E4A' }} />
                        <p>+20 1024607316</p>
                    </div>
                    <div className="the-row">
                        <MailIcon style={{ color: '#EA8E4A' }} />
                        <p>Info@mci.com </p>
                    </div>
                </div>
                <div className="vl"></div>
                <div className='the-partition'>
                    <h3>Marketing And Sales Department</h3>
                    <div className="the-row">
                        <CallIcon style={{ color: '#EA8E4A' }} />
                        <p>+20 1000092622</p>
                    </div>
                    <div className="the-row">
                        <CallIcon style={{ color: '#EA8E4A' }} />
                        <p>+20 1024607316</p>
                    </div>
                    <div className='social-icons'>
                        <FacebookIcon className='social-icons-style' onClick={() => window.open('https://www.facebook.com')} />
                        <TwitterIcon className='social-icons-style' onClick={() => window.open('https://www.twitter.com')} />
                        <Link relative={false} href="mailto:kafro@gmail.com">
                            <MailIcon className='social-icon-style' />
                        </Link>
                    </div>
                </div>
                <div className="vl"></div>
                <div className='the-partition'>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3447.2930116538887!2d31.7349424151194!3d30.22871968181564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzDCsDEzJzQzLjQiTiAzMcKwNDQnMTMuNyJF!5e0!3m2!1sen!2seg!4v1611002603337!5m2!1sen!2seg" title="title" className="google-maps" frameBorder="0" allowFullScreen="" tabIndex="0"></iframe>
                </div>
            </div>
            <div className="copyright">
                    <p>© 2021 Crafted Internet- All rights reserved</p>
            </div>
        </>
    );
}