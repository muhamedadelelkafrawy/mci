import React from 'react';
import MainLayout from 'reactor/layout/components/layout';
import Header from './Header/Header';
import Footer from './Footer/Footer';

export default function Layout(props) {
    return (
        <MainLayout>
            <Header />
            {props.children}
            <Footer />
        </MainLayout>
    )
} 