import React from 'react';
import Link from 'reactor/components/link';
import '../Header/Header.scss';
import introImage from 'assets/images/national-cancer-institute-ZeitGGtlwzI-unsplash.jpg';
import introImage2 from 'assets/images/mainmain.jpeg';
import introImage3 from 'assets/images/bg3.jpg';
import OwlCarousel from 'react-owl-carousel';
import FacebookIcon from '@material-ui/icons/Facebook';
import CallIcon from '@material-ui/icons/Call';
import MailIcon from '@material-ui/icons/Mail';

export default function MainSlider() {
    const options = {
        loop: true,
        margin: 2,
        nav: false,
        dots: false,
        lazyLoad: true,
        autoplay: true,
        smartSpeed: 1500,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        },
    };

    return (
        <>
            <div className="header-container">
                <OwlCarousel
                    className="owl-theme "
                    {...options}
                >
                    <div className='item3'>
                        <img className='intro-img' src={introImage} alt="intro" />
                    </div>
                    <div className='item3'>
                        <img className='intro-img' src={introImage2} alt="intro" />
                    </div>
                    <div className='item3'>
                        <img className='intro-img' src={introImage3} alt="intro" />
                    </div>
                </OwlCarousel>

                <div className='center-part'>
                    <h1 className='first-row' >MCI</h1>
                    <h1 className='second-row' >We are best manufactures of Closure caps & Pilfer Proof & Flip Off For pharmaceuticals and food products in Egypt and Africa  </h1>
                </div>

                <div className='social-media'>
                    <FacebookIcon className='social-icon-style' onClick={() => window.open('https://www.facebook.com')} />
                    <Link relative={false} href="tel:+20 1000092622">
                        <CallIcon className='social-icon-style'/>
                    </Link>
                    <Link relative={false} href="mailto:kafro@gmail.com">
                        <MailIcon className='social-icon-style' />
                    </Link>
                </div>
                <div className='for-more' >
                    <h3>FOR MORE</h3>
                    <h3 className='animate__animated animate__fadeIn animate__infinite'><i className="arrow bright1 down"></i></h3>
                    <h3 className='animate__animated animate__fadeIn animate__infinite'><i className="arrow bright2 down"></i></h3>
                    <h3 className='animate__animated animate__fadeIn animate__infinite'><i className="arrow bright3 down"></i></h3>
                </div>
            </div>
        </>
    )
}
