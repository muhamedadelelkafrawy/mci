import React from 'react';
import Link from 'reactor/components/link';
import Helmet from 'reactor/components/helmet';
import '../Header/NewHeader.scss';
import logo from 'assets/images/logo.png';
import Is from '@flk/supportive-is';
import MobileHeader from './mobileHeader';

export default function NewHeader() {

    if (Is.mobile.any() && !Is.mobile.ipad()) return <MobileHeader />

    return (
        <>
            <Helmet title="Home Page" description="my desc" bodyClass="home-page" id="test" />
            
            <div className="new-header-container">
                <div className="left-part">
                    <Link className='logo-container' href="/">
                        <img className='logo' src={logo} alt="Logo" />
                    </Link>
                </div>
                <div className="right-part">
                    <Link className='header-links' to="/">
                        <h3 className='header-links-font'>HOME</h3>
                    </Link>
                    <Link className='header-links' to="/product">
                        <h3 className='header-links-font'>PRODUCTS</h3>
                    </Link>
                    <Link className='header-links' to="/about-us">
                        <h3 className='header-links-font'>ABOUT US</h3>
                    </Link>
                    <Link className='header-links' to="/quality">
                        <h3 className='header-links-font'>QUALITY</h3>
                    </Link>
                    <Link className='header-links' to="/contact-us">
                        <h3 className='header-links-font'>CONTACT US</h3>
                    </Link>
                </div>
            </div>
        </>
    )
}

